import java.io.*;
import java.util.ArrayList;

public class FileUtils {

    public ArrayList<String> loadFile(String inputFile) {

        ArrayList<String> lines = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile))) { //  с автоматическим закрытием потока
            String line;
            // цикл для считывания всех строк по одной строке
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public void writeInFile(String outputFile, String result){

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {

            writer.write(result);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't write");

        }
    }


}


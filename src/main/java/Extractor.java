import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Extractor {


    public String extract(ArrayList<String> lines){

        String sourceString = lines.get(0); // если будет несколько строк, то нужен цикл
        String regExp = "\\+[\\d]\\([\\d]{3}\\)\\s\\d{3}(\\s\\d{2}){2}";
        String result = "";

        Pattern pattern = Pattern.compile(regExp); // скомпилированное представление regex
        Matcher matcher = pattern.matcher(sourceString); // согласуем строку с регуярным выражением и будем хранить в matcher результаты
        while(matcher.find()) // проходимся по согласованной строке и ищем наши совпадения с regex
        {
            System.out.println(matcher.group().replaceAll("[^0-9]", ""));
            result += matcher.group().replaceAll("[^0-9]", "") + "\n"; // берем группу символов из строки соотвеетствующей regex
        }

        return result;
    }


}

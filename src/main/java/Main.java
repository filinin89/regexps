import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        String inputFilePath = ".\\src\\main\\resources\\text.txt";
        String outputFilePath = ".\\src\\main\\resources\\numbers.txt";

        // считывание с файла
        FileUtils fUtils = new FileUtils();
        ArrayList<String> lines = fUtils.loadFile(inputFilePath);

        // извлечение номеров
        Extractor extractor = new Extractor();
        String result = extractor.extract(lines);

        // запись в файл
        fUtils.writeInFile(outputFilePath, result);

    }
}
